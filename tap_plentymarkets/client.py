"""REST client handling, including PlentymarketsStream base class."""

import datetime
from pathlib import Path
from time import time
from typing import Any, Dict, Optional

import requests
from singer_sdk.authenticators import BearerTokenAuthenticator
from singer_sdk.streams import RESTStream


class PlentymarketsStream(RESTStream):
    """Plentymarkets stream class."""

    expires_in = None
    access_token = None
    records_jsonpath = "$[*]"

    @property
    def url_base(self) -> str:
        return f"{self.config['api_url']}/rest"

    def get_token(self):
        now = round(time())
        if (
            not self.access_token
            or (not self.expires_in)
            or ((self.expires_in - now) < 60)
        ):
            s = requests.Session()
            payload = {
                "username": self.config.get("username"),
                "password": self.config.get("password"),
            }
            login = s.post(f"{self.url_base}/login", json=payload).json()
            self.access_token = login["access_token"]
            self.expires_in = login["expires_in"] + now
        return self.access_token

    @property
    def authenticator(self) -> BearerTokenAuthenticator:
        """Return a new authenticator object."""
        return BearerTokenAuthenticator.create_for_stream(self, token=self.get_token())

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        next_page_token = None

        res = response.json()
        if "isLastPage" in res:
            if res["isLastPage"] is False:
                next_page_token = res["page"] + 1
            else:
                return None    
        else:
            return None

        return next_page_token

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token

        if "starting_replication_value" in self.stream_state:
            if self.stream_state["starting_replication_value"] is not None:
                updated_from = self.get_starting_timestamp(context).strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                )
                updated_to = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")

                if self.tap_stream_id == "items_stock" or self.tap_stream_id == "orders":
                    params["updatedAtFrom"] = updated_from
                    params["updatedAtTo"] = updated_to

                if self.tap_stream_id in ["items","items_variations"]:
                    params["updatedBetween"] = f"{updated_from},{updated_to}"

                if self.tap_stream_id == "orders":
                    params["updatedAt"] = updated_from

        return params

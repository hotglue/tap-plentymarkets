"""Stream type classes for tap-plentymarkets."""

from pathlib import Path
from typing import Optional

from singer_sdk import typing as th

from tap_plentymarkets.client import PlentymarketsStream


class OrdersStream(PlentymarketsStream):
    """Define custom stream."""

    name = "orders"
    path = "/orders"
    primary_keys = ["id"]
    records_jsonpath = "$.entries[*]"
    replication_key = "updatedAt"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("referrerId", th.NumberType),
        th.Property("roundTotalsOnly", th.BooleanType),
        th.Property("numberOfDecimals", th.NumberType),
        th.Property("statusName", th.StringType),
        th.Property("plentyId", th.NumberType),
        th.Property("typeId", th.NumberType),
        th.Property("lockStatus", th.StringType),
        th.Property("locationId", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("statusId", th.NumberType),
        th.Property("ownerId", th.StringType),
        th.Property("relations", th.CustomType({"type": ["array", "string"]})),
        th.Property("properties", th.ArrayType(
            th.ObjectType(
                th.Property('orderId', th.NumberType),
                th.Property('typeId', th.NumberType),
                th.Property('value', th.StringType),
                th.Property("createdAt", th.DateTimeType),
                th.Property("updatedAt", th.DateTimeType),
            )
        )),
        th.Property("dates", th.ArrayType(
            th.ObjectType(
                th.Property('orderId', th.NumberType),
                th.Property('typeId', th.NumberType),
                th.Property("date", th.DateTimeType),
                th.Property("createdAt", th.DateTimeType),
                th.Property("updatedAt", th.DateTimeType),
            )
        )),
        th.Property("amounts", th.CustomType({"type": ["array", "string"]})),
        th.Property("orderReferences", th.CustomType({"type": ["array", "string"]})),
        th.Property("orderItems", th.CustomType({"type": ["array", "string"]})),
        th.Property("addressRelations", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "orderItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderId", th.NumberType),
                    th.Property("typeId", th.NumberType),
                    th.Property("referrerId", th.NumberType),
                    th.Property("itemVariationId", th.NumberType),
                    th.Property("quantity", th.NumberType),
                    th.Property("countryVatId", th.NumberType),
                    th.Property("vatField", th.NumberType),
                    th.Property("vatRate", th.NumberType),
                    th.Property("orderItemName", th.StringType),
                    th.Property("attributeValues", th.StringType),
                    th.Property("shippingProfileId", th.NumberType),
                    th.Property("createdAt", th.DateTimeType),
                    th.Property("updatedAt", th.DateTimeType),
                    th.Property("id", th.NumberType),
                    th.Property(
                        "amounts", th.CustomType({"type": ["array", "string"]})
                    ),
                    th.Property(
                        "properties", th.CustomType({"type": ["array", "string"]})
                    ),
                    th.Property(
                        "orderProperties", th.CustomType({"type": ["array", "string"]})
                    ),
                )
            ),
        ),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        """Return a context dictionary for child streams."""
        return {
            "order_id": record["id"],
        }

#Temporarily disabling this stream
class OrderItemsStream(PlentymarketsStream):
    """Define custom stream."""

    name = "order_items"
    path = "/orders/{order_id}"
    primary_keys = ["id"]
    parent_stream_type = OrdersStream
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property(
            "orderItems",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderId", th.NumberType),
                    th.Property("typeId", th.NumberType),
                    th.Property("referrerId", th.NumberType),
                    th.Property("itemVariationId", th.NumberType),
                    th.Property("quantity", th.NumberType),
                    th.Property("countryVatId", th.NumberType),
                    th.Property("vatField", th.NumberType),
                    th.Property("vatRate", th.NumberType),
                    th.Property("orderItemName", th.StringType),
                    th.Property("attributeValues", th.StringType),
                    th.Property("shippingProfileId", th.NumberType),
                    th.Property("createdAt", th.DateTimeType),
                    th.Property("updatedAt", th.DateTimeType),
                    th.Property("id", th.NumberType),
                    th.Property(
                        "amounts", th.CustomType({"type": ["array", "string"]})
                    ),
                    th.Property(
                        "properties", th.CustomType({"type": ["array", "string"]})
                    ),
                    th.Property(
                        "orderProperties", th.CustomType({"type": ["array", "string"]})
                    ),
                )
            ),
        ),
        th.Property(
            "properties",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderId", th.NumberType),
                    th.Property("typeId", th.NumberType),
                    th.Property("value", th.DateTimeType),
                    th.Property("updatedAt", th.DateTimeType),
                )
            ),
        ),
        th.Property(
            "addressRelations",
            th.ArrayType(
                th.ObjectType(
                    th.Property("orderId", th.NumberType),
                    th.Property("typeId", th.NumberType),
                    th.Property("addressId", th.NumberType),
                )
            ),
        ),
        th.Property("amounts", th.CustomType({"type": ["array", "string"]})),
        th.Property("statusName", th.StringType),
        th.Property("plentyId", th.NumberType),
        th.Property("typeId", th.NumberType),
        th.Property("lockStatus", th.StringType),
        th.Property("locationId", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("statusId", th.NumberType),
        th.Property("ownerId", th.StringType),
        th.Property("relations", th.CustomType({"type": ["array", "string"]})),
        th.Property("dates", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class ItemsStockStream(PlentymarketsStream):
    name = "items_stock"
    primary_keys = ["id"]
    records_jsonpath = "$.entries[*]"
    path = "/stockmanagement/stock"
    replication_key = "updatedAt"
    schema = th.PropertiesList(
        th.Property("statusName", th.StringType),
        th.Property("warehouseId", th.NumberType),
        th.Property("itemId", th.NumberType),
        th.Property("variationId", th.NumberType),
        th.Property("stockPhysical", th.NumberType),
        th.Property("reservedStock", th.NumberType),
        th.Property("reservedEbay", th.NumberType),
        th.Property("reorderDelta", th.NumberType),
        th.Property("stockNet", th.NumberType),
        th.Property("reordered", th.NumberType),
        th.Property("reservedBundle", th.NumberType),
        th.Property("averagePurchasePrice", th.NumberType),
        th.Property("updatedAt", th.DateTimeType),
    ).to_dict()


class WarehousesStream(PlentymarketsStream):
    name = "warehouses"
    primary_keys = ["id"]
    path = "/stockmanagement/warehouses"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("note", th.StringType),
        th.Property("typeId", th.NumberType),
        th.Property("onStockAvailability", th.NumberType),
        th.Property("outOfStockAvailability", th.NumberType),
        th.Property("splitByShippingProfile", th.BooleanType),
        th.Property("storageLocationType", th.StringType),
        th.Property("storageLocationZone", th.NumberType),
        th.Property("repairWarehouseId", th.NumberType),
        th.Property("isInventoryModeActive", th.BooleanType),
        th.Property("logisticsType", th.StringType),
        th.Property("average_price_source", th.StringType),
        th.Property("reorder_level_dynamic", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
    ).to_dict()


class ItemsStream(PlentymarketsStream):
    name = "items"
    primary_keys = ["id"]
    path = "/items"
    replication_key = "updatedAt"
    records_jsonpath = "$.entries[*]"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("position", th.NumberType),
        th.Property("manufacturerId", th.NumberType),
        th.Property("stockType", th.NumberType),
        th.Property("add_cms_page", th.StringType),
        th.Property("storeSpecial", th.NumberType),
        th.Property("condition", th.NumberType),
        th.Property("amazonFedas", th.StringType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("free1", th.StringType),
        th.Property("free2", th.StringType),
        th.Property("free3", th.StringType),
        th.Property("free4", th.StringType),
        th.Property("free5", th.StringType),
        th.Property("free6", th.StringType),
        th.Property("free7", th.StringType),
        th.Property("free8", th.StringType),
        th.Property("free9", th.StringType),
        th.Property("free10", th.StringType),
        th.Property("free11", th.StringType),
        th.Property("free12", th.StringType),
        th.Property("free13", th.StringType),
        th.Property("free14", th.StringType),
        th.Property("free15", th.StringType),
        th.Property("free16", th.StringType),
        th.Property("free17", th.StringType),
        th.Property("free18", th.StringType),
        th.Property("free19", th.StringType),
        th.Property("free20", th.StringType),
        th.Property("customsTariffNumber", th.StringType),
        th.Property("producingCountryId", th.NumberType),
        th.Property("revenueAccount", th.NumberType),
        th.Property("couponRestriction", th.NumberType),
        th.Property("flagOne", th.NumberType),
        th.Property("flagTwo", th.NumberType),
        th.Property("ageRestriction", th.NumberType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("amazonProductType", th.NumberType),
        th.Property("ebayPresetId", th.NumberType),
        th.Property("ebayCategory", th.NumberType),
        th.Property("ebayCategory2", th.NumberType),
        th.Property("ebayStoreCategory", th.NumberType),
        th.Property("ebayStoreCategory2", th.NumberType),
        th.Property("amazonFbaPlatform", th.NumberType),
        th.Property("feedback", th.NumberType),
        th.Property("gimahhot", th.StringType),
        th.Property("maximumOrderQuantity", th.NumberType),
        th.Property("isSubscribable", th.BooleanType),
        th.Property("rakutenCategoryId", th.NumberType),
        th.Property("isShippingPackage", th.BooleanType),
        th.Property("conditionApi", th.NumberType),
        th.Property("isSerialNumber", th.BooleanType),
        th.Property("isShippableByAmazon", th.BooleanType),
        th.Property("ownerId", th.StringType),
        th.Property("itemType", th.StringType),
        th.Property("mainVariationId", th.NumberType),
        th.Property("texts", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()


class ItemsVariationsStream(PlentymarketsStream):
    """Define custom stream."""

    name = "items_variations"
    path = "/items/variations"
    primary_keys = ["id"]
    records_jsonpath = "$.entries[*]"
    replication_key = "updatedAt"
    schema = th.PropertiesList(
        th.Property("id", th.NumberType),
        th.Property("isMain", th.BooleanType),
        th.Property("mainVariationId", th.CustomType({"type": ["number", "string"]})),
        th.Property("itemId", th.NumberType),
        th.Property("categoryVariationId", th.NumberType),
        th.Property("marketVariationId", th.NumberType),
        th.Property("clientVariationId", th.NumberType),
        th.Property("salesPriceVariationId", th.NumberType),
        th.Property("supplierVariationId", th.NumberType),
        th.Property("warehouseVariationId", th.NumberType),
        th.Property("position", th.CustomType({"type": ["number", "string"]})),
        th.Property("isActive", th.BooleanType),
        th.Property("number", th.StringType),
        th.Property("model", th.StringType),
        th.Property("externalId", th.StringType),
        th.Property("parentVariationId", th.CustomType({"type": ["number", "string"]})),
        th.Property("parentVariationQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("availability", th.NumberType),
        th.Property("estimatedAvailableAt", th.DateTimeType),
        th.Property("purchasePrice", th.CustomType({"type": ["number", "string"]})),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("relatedUpdatedAt", th.DateTimeType),
        th.Property("priceCalculationId", th.CustomType({"type": ["number", "string"]})),
        th.Property("priceCalculationUUID", th.CustomType({"type": ["number", "string"]})),
        th.Property("picking", th.StringType),
        th.Property("stockLimitation", th.NumberType),
        th.Property("isVisibleIfNetStockIsPositive", th.BooleanType),
        th.Property("isInvisibleIfNetStockIsNotPositive", th.BooleanType),
        th.Property("isAvailableIfNetStockIsPositive", th.BooleanType),
        th.Property("isUnavailableIfNetStockIsNotPositive", th.BooleanType),
        th.Property("mainWarehouseId", th.NumberType),
        th.Property("maximumOrderQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("minimumOrderQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("intervalOrderQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("availableUntil", th.DateTimeType),
        th.Property("releasedAt", th.DateTimeType),
        th.Property("unitCombinationId", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("weightG", th.NumberType),
        th.Property("weightNetG", th.NumberType),
        th.Property("widthMM", th.NumberType),
        th.Property("lengthMM", th.NumberType),
        th.Property("heightMM", th.NumberType),
        th.Property("extraShippingCharge1", th.CustomType({"type": ["number", "string"]})),
        th.Property("extraShippingCharge2", th.CustomType({"type": ["number", "string"]})),
        th.Property("unitsContained", th.NumberType),
        th.Property("palletTypeId", th.CustomType({"type": ["number", "string"]})),
        th.Property("packingUnits", th.CustomType({"type": ["number", "string"]})),
        th.Property("packingUnitTypeId", th.CustomType({"type": ["number", "string"]})),   
        th.Property("transportationCosts", th.NumberType),
        th.Property("storageCosts", th.NumberType),
        th.Property("customs", th.CustomType({"type": ["number", "string"]})),
        th.Property("operatingCosts", th.CustomType({"type": ["number", "string"]})),
        th.Property("vatId", th.NumberType),
        th.Property("bundleType", th.CustomType({"type": ["number", "string"]})),
        th.Property("automaticClientVisibility", th.NumberType),
        th.Property("isHiddenInCategoryList", th.BooleanType),
        th.Property("defaultShippingCosts", th.NumberType),
        th.Property("mayShowUnitPrice", th.BooleanType),
        th.Property("movingAveragePrice", th.NumberType),
        th.Property("propertyVariationId", th.StringType),
        th.Property("automaticListVisibility", th.NumberType),
        th.Property("isVisibleInListIfNetStockIsPositive", th.BooleanType),
        th.Property("isInvisibleInListIfNetStockIsNotPositive", th.BooleanType),
        th.Property("singleItemCount", th.NumberType),
        th.Property("availabilityUpdatedAt", th.DateTimeType),
        th.Property("tagVariationId", th.StringType),
        th.Property("hasCalculatedBundleWeight", th.BooleanType),
        th.Property("hasCalculatedBundleNetWeight", th.BooleanType),
        th.Property("hasCalculatedBundlePurchasePrice", th.BooleanType),
        th.Property("hasCalculatedBundleMovingAveragePrice", th.BooleanType),
        th.Property("customsTariffNumber", th.StringType),
        th.Property("salesRank", th.CustomType({"type": ["number", "string"]})),
    ).to_dict()

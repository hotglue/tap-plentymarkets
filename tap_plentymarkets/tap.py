"""Plentymarkets tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_plentymarkets.streams import (
    ItemsStockStream,
    ItemsStream,
    OrderItemsStream,
    OrdersStream,
    PlentymarketsStream,
    WarehousesStream,
    ItemsVariationsStream,
)
#Temporarily removed OrderItems stream
STREAM_TYPES = [
    OrdersStream,
    ItemsStockStream,
    WarehousesStream,
    ItemsStream,
    ItemsVariationsStream,
]


class TapPlentymarkets(Tap):
    """Plentymarkets tap class."""

    name = "tap-plentymarkets"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "username",
            th.StringType,
            required=True,
        ),
        th.Property(
            "password",
            th.StringType,
            required=True,
        ),
        th.Property(
            "api_url",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapPlentymarkets.cli()
